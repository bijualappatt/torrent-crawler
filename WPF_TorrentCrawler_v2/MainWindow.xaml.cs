﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_TorrentCrawler_v2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Crawler crawl = new Crawler();

        DataTable items;
        int page = 1;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Loading(bool complete)
        {
            if (complete)
            {
                btnSearch.Content = "SEARCH";
                btnSearch.IsEnabled = true;
                btnPrev.IsEnabled = true;
                btnNext.IsEnabled = true;
            }
            else
            {
                btnSearch.Content = "Crawling...";
                btnSearch.IsEnabled = false;
                btnPrev.IsEnabled = false;
                btnNext.IsEnabled = false;
            }
        }

        private void Builder()
        {
            if (items.Rows.Count > 0)
            {
                foreach (DataRow row in items.Rows)
                    SPContainer.Children.Add(new CustomContent(row));
                SPPager.Visibility = Visibility.Visible;
            }
            else
            {
                page = 1;
                lblPage.Content = page.ToString();
                SPPager.Visibility = Visibility.Hidden;
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            page = 1;
            SPContainer.Children.Clear();
            if (!String.IsNullOrWhiteSpace(txtSearch.Text))
            {
                Loading(false);
                items = crawl.Search(txtSearch.Text, 1);
                Loading(true);
                Builder();
            }
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            if (page > 1)
            {
                page--;
                lblPage.Content = page.ToString();

                SPContainer.Children.Clear();
                Loading(false);

                items = new DataTable();
                items = crawl.Search(txtSearch.Text, page);
                Loading(true);
                Builder();
            }
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            page++;
            lblPage.Content = page.ToString();

            SPContainer.Children.Clear();
            Loading(false);

            items = new DataTable();
            items = crawl.Search(txtSearch.Text, page);
            Loading(true);
            Builder();
        }
    }
}
