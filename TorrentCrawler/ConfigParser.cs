﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;

namespace TorrentCrawler
{
    class ConfigParser
    {
        public static ConfigProxies Proxies = new ConfigProxies();
    }

    class ConfigProxies
    {
        private NameValueCollection Proxies = (NameValueCollection)ConfigurationManager.GetSection("Proxies");

        public Dictionary<int, string> ToDictionary()
        {
            if (Proxies.HasKeys())
            {
                Dictionary<int, string> result = new Dictionary<int, string>();
                foreach (var key in Proxies.AllKeys)
                    result.Add(Convert.ToInt32(key), Proxies[key].ToString());

                return result;
            }
            return null;
        }

    }
}
