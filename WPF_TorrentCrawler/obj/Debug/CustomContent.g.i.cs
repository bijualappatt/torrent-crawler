﻿#pragma checksum "..\..\CustomContent.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "AFD9C0D3D1E5A83B0783805A86F01A34"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WPF_TorrentCrawler;


namespace WPF_TorrentCrawler {
    
    
    /// <summary>
    /// CustomContent
    /// </summary>
    public partial class CustomContent : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\CustomContent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lblTitle;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\CustomContent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblReso;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\CustomContent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSize;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\CustomContent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSeeders;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\CustomContent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblLeechers;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\CustomContent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnMagnet;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\CustomContent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTorrent;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WPF_TorrentCrawler;component/customcontent.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CustomContent.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lblTitle = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.lblReso = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.lblSize = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.lblSeeders = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.lblLeechers = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.btnMagnet = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\CustomContent.xaml"
            this.btnMagnet.Click += new System.Windows.RoutedEventHandler(this.btnMagnet_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnTorrent = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\CustomContent.xaml"
            this.btnTorrent.Click += new System.Windows.RoutedEventHandler(this.btnTorrent_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

