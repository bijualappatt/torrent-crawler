﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace WPF_TorrentCrawler
{
    class Crawler
    {
        DataTable ResultTable;
        HtmlDocument html;
        string url = @"https://www.kat.cr/usearch/{0}/{1}/?field=seeders&sorder=desc";

        private void BuildHeaderRequest(Uri url, HttpWebRequest req)
        {
            req.UserAgent = @"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            req.ContentType = "text/html; charset=UTF-8";
            req.Headers.Add("Accept-Encoding: gzip, deflate");
            req.Headers.Add("Accept-Language: en-US,en,q=0.5");
        }

        private string DecompressGZip(Stream data, string charset)
        {
            MemoryStream decompressed = new MemoryStream();
            Encoding encoding = Encoding.GetEncoding(charset);
            GZipStream gzip = new GZipStream(data, CompressionMode.Decompress);
            gzip.CopyTo(decompressed);
            return encoding.GetString(decompressed.GetBuffer(), 0, (int)decompressed.Length);
        }

        private string GetCharset(string contenttype)
        {
            if (Regex.IsMatch(contenttype, @"charset=\S+"))
                return Regex.Match(contenttype, @"charset=\S+").ToString().Split('=')[1];
            return "utf-8";
        }

        private string GetSiteRawHtml(string url)
        {
            UriBuilder toCrawl = new UriBuilder(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(toCrawl.Uri);
            BuildHeaderRequest(toCrawl.Uri, request);

            try
            {
                WebResponse response = request.GetResponse();
                string[] absUri = response.ResponseUri.AbsoluteUri.Replace("%20", " ").Split('/');

                if (!Regex.IsMatch(absUri[absUri.Length - 2], "\\d+"))
                    throw new WebException("(404) Not Found.");

                if (((HttpWebResponse)response).ContentEncoding.ToString().ToUpper() == "GZIP")
                {
                    string decompressed = DecompressGZip(response.GetResponseStream(), GetCharset(response.ContentType));
                    response.Close();
                    return decompressed;
                }

                else
                {

                    Stream data = response.GetResponseStream();
                    StreamReader reader = new StreamReader(data);
                    response.Close();
                    return reader.ReadToEnd();
                }
            }

            catch (WebException e)
            {
                return e.Message;
            }
        }

        private MemoryStream DecompressGZipInStream(Stream data, string charset)
        {
            MemoryStream decompressed = new MemoryStream();
            Encoding encoding = Encoding.GetEncoding(charset);
            GZipStream gzip = new GZipStream(data, CompressionMode.Decompress);
            gzip.CopyTo(decompressed);
            return decompressed;
        }

        private string GetTitle(string innertext)
        {
            return innertext.Split('\n')[11].Trim();
        }

        private string GetNumberOfFiles(string innerhtml)
        {
            string constpattern = "<td class=\"center\">";
            return Regex.Match(innerhtml, constpattern + "\\d+").ToString().Substring(constpattern.Length);
        }

        private string GetFileSize(string innerhtml)
        {
            string constpattern = "<td class=\"nobr center\">";
            string size = Regex.Match(innerhtml, constpattern + "\\S+").ToString().Substring(constpattern.Length);

            constpattern = constpattern + size + " <span>";
            size += Regex.Match(innerhtml, constpattern + "\\w+").ToString().Substring(constpattern.Length);
            return size;
        }

        private string GetMagnetLink(string innerhtml)
        {
            return Regex.Match(innerhtml, "magnet:\\S+").ToString().Replace("\"", "");
        }

        private string GetTorrentLink(string innerhtml)
        {
            return Regex.Match(innerhtml, "torcache\\.net\\S+").ToString().Replace("\"", "");
        }

        private string GetTotalSeeders(string innerhtml)
        {
            string constpattern = "<td class=\"green center\">";
            return Regex.Match(innerhtml, constpattern + "\\d+").ToString().Substring(constpattern.Length);
        }

        private string GetTotalLeechers(string innerhtml)
        {
            string constpattern = "<td class=\"red lasttd center\">";
            string a = Regex.Match(innerhtml, constpattern + "\\d+").ToString().Substring(constpattern.Length);
            return Regex.Match(innerhtml, constpattern + "\\d+").ToString().Substring(constpattern.Length);
        }

        public DataTable Search(string key, int page)
        {
            string innerhtml;
            string source = WebUtility.HtmlDecode(GetSiteRawHtml(string.Format(url, key.Replace(" ", "%20"), page)));

            ResultTable = new DataTable();
            ResultTable.Columns.Add("Title");
            ResultTable.Columns.Add("Resolution");
            ResultTable.Columns.Add("Files");
            ResultTable.Columns.Add("Size");
            ResultTable.Columns.Add("Magnet");
            ResultTable.Columns.Add("Torrent");
            ResultTable.Columns.Add("Seeders");
            ResultTable.Columns.Add("Leechers");

            html = new HtmlDocument();
            html.LoadHtml(source);
            IEnumerable<HtmlNode> nodes = html.DocumentNode.Descendants().Where(x => x.Name == "tr"
                            && x.Attributes.Contains("class")
                            && (x.Attributes["class"].Value.Split().Contains("odd") || x.Attributes["class"].Value.Split().Contains("even")));
            foreach (HtmlNode node in nodes)
            {
                innerhtml = node.InnerHtml;
                ResultTable.Rows.Add(new object[] {
                            GetTitle(node.InnerText),
                            Regex.Match(innerhtml, @"((\d{3,4})(p|P)|(\d{3,4}x\d{3,4}))").ToString(),
                            GetNumberOfFiles(innerhtml),
                            GetFileSize(innerhtml),
                            GetMagnetLink(innerhtml),
                            GetTorrentLink(innerhtml),
                            GetTotalSeeders(innerhtml),
                            GetTotalLeechers(innerhtml)
                        });
            }
            return ResultTable;
        }

        public void DownloadTorrent(string url, string filename)
        {
            UriBuilder toDownload = new UriBuilder(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(toDownload.Uri);
            BuildHeaderRequest(toDownload.Uri, request);

            WebResponse response = request.GetResponse();
            string[] absUri = response.ResponseUri.AbsoluteUri.Replace("%20", " ").Split('/');
            byte[] streamInByte = DecompressGZipInStream(response.GetResponseStream(), GetCharset(response.ContentType)).ToArray();
            response.Close();

            using (BinaryWriter writer = new BinaryWriter(new FileStream(filename, FileMode.Create)))
            {
                writer.Write(streamInByte);
            }
        }
    }
}
